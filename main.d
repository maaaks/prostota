import vibe.d;

import prostota.config;
import prostota.routing;
import std.conv;
import std.getopt;
import std.stdio;

void main(string[] args) {
	// Parse options using std.getopt
	string configFilePath = null;
	getopt(args,
		std.getopt.config.caseSensitive,
		std.getopt.config.passThrough,
		"c|config", &configFilePath,
	);
	
	// Load Config
	if (configFilePath is null) {
		writeln("You must pass --config option to start the server.");
		return;
	}
	Config.initialize(configFilePath);
	
	// Initialize vibe.d
	lowerPrivileges();
	runEventLoop();
}

shared static this() {
	// Kill previous runs of Prostota
	while (true) {
		auto result = std.process.execute(["ps", "h", "-o", "pid", "-C", "prostota"]);
		auto strPids = result.output.splitLines();
		if (strPids.length == 1)
			break;
		foreach (string strPid; strPids) {
			int pid = std.conv.to!int(strPid.strip());
			if (pid != std.process.getpid())
				std.process.execute(["kill", "-9", strPid]);
		}
	}

	auto http = new HTTPServerSettings;
	http.hostName = "prostota";
	http.bindAddresses = ["127.0.0.1"];
	http.port = 1099;

	auto router = new URLRouter;
	router.get("*", &index);

	listenHTTP(http, router);
}
