module prostota.response;

public import prostota.aux;
public import prostota.runtime;

import std.conv;
import vibe.core.file;
import vibe.http.server;

/**
	Response is the data that will be returned to user.
	
	It can be formed in to places:
		- an Engine creates Response for given URL;
		- a Parser converts or modifies the Engine's Response and create a new one,
			which goes to the user.
	Both Engine and Parser can fill Response with additional information
	what will be sent in HTTP headers: MIME type, cache invalidation period, size.
	When you create a Response from a string or from a file, its size is detected automatically.
	
	There are different types of Responses, you must instantiate one of them
	with Response.fromString(), Response.fromStream(), or Response.fromFile().
	Any of them can be later converted to either string or stream,
	but it is recommended to use that type of Response that corresponds to your real data,
	e.g. if an Engine reads content from a file on disk, it's better for it to use Response.fromFile().
	In this case, if Parser works with streams, not files, then no unneeded conversions will be done,
	and Parser will work with Engine's output in a pipeline style, without additional variables.
	
	You may ask: why is Response an abstract class, not an interface?
	I would answer: because in Parser.parse() I need to get exact type through typeid(response),
	and interfaces don't seem to support this.
 */
abstract class Response
{
	/// Returns the whole content of the Response as a string.
	override string toString();
	
	/// Returns vibe.d-compatible InputStream which can be used to read Response's content.
	InputStream toStream();
	
	/// Sets HTTP headers and sends data to vibe.d's HTTPServerResponse object.
	void applyToHttp(HTTPServerResponse httpRes);
	
	/// Creates a StringResponse using given string.
	final static Response fromString(in string data) {
		return new StringResponse(data);
	}
	
	/// Creates a StreamResponse from given ready vibe.d's InputStream.
	final static Response fromStream(InputStream data) {
		return new StreamResponse(data);
	}
	
	/// Creates a StreamResponse from given standard InputStream.
	final static Response fromStream(std.stream.InputStream data) {
		auto wrapper = new StdStreamWrapper(data);
		return new StreamResponse(wrapper);
	}
	
	/// Creates a StreamResponse from given file by its filename.
	final static Response fromFile(in string fileName) {
		auto file = openFile(std.path.buildNormalizedPath(fileName));
		return new StreamResponse(file);
	}
}


/**
	A Response created from a string.
	
	If parser wants to retrieve it as a string, he gets it.
	If parser wants to retrieve it as a stream, he gets a StringStreamingWrapper.
 */
class StringResponse : Response
{
	private string data;
	
	private this(string data) {
		this.data = data;
	}
	
	/// Just return the string as is.
	override string toString() {
		return data;
	}
	
	/// Wrap the string into StringStreamingWrapper.
	override InputStream toStream() {
		return new StringStreamingWrapper(data);
	}
	
	/// Write the whole string at once to the body.
	override void applyToHttp(HTTPServerResponse httpRes) {
		httpRes.bodyWriter.write(data);
	}
}


/**
	A Response created from a stream.
	
	When being retrieved as a string, it converts data to ubyte[] and then to string.
 */
class StreamResponse : Response
{
	private InputStream data;
	
	this(InputStream data) {
		this.data = data;
	}
	
	this(ChunkedInputStream data) {
		this.data = data;
	}
	
	override string toString() {
		string result;
		while (!data.empty) {
			ubyte[] buffer = new ubyte[data.leastSize];
			data.read(buffer);
			result ~= buffer;
			
		}
		return result;
	}
	
	override InputStream toStream() {
		return data;
	}
	
	override void applyToHttp(HTTPServerResponse httpRes) {
		if ("Content-Length" !in Runtime.headers)
			Runtime.headers["Content-Length"] = to!string(data.leastSize);
		httpRes.bodyWriter.write(data);
	}
}
