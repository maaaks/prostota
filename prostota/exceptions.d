module prostota.exceptions;

import vibe.http.status;

class HttpException : Exception
{
	int code;
	
	this(in int code) {
		this.code = code;
		super(httpStatusText(code));
	}
	
	this(in int code, in string message) {
		this.code = code;
		super(message);
	}
}

class HttpRedirect : Exception
{
	int code;
	
	this(in int code, in string location) {
		this.code = code;
		super(location);
	}
}
	
class Http(int Code) : HttpException
if (Code!=201 && Code!=202 && !(300<=Code && Code<=308))
{
	this() {
		super(Code);
	}
	this(in string message) {
		super(Code, message);
	}
}

class Http(int Code) : HttpRedirect
if (Code==201 || Code==202 || (300<=Code && Code<=308))
{
	this(in string location) {
		super(Code, location);
	}
}

/**
	Base for custom exceptions thrown from syntax parsers.
 */
class ParseException : Exception
{
	this() { super("Parse Error"); }
	this(in string msg) { super(msg); }
}