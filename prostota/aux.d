module prostota.aux;

import std.exception;
import std.stdio;
static import std.stream;
import vibe.core.stream;

/**
	Aux class that wraps a string into a vibe.d-compatible InputStream.
 */
class StringStreamingWrapper : InputStream
{
	private string str;
	private bool hasBeenRead;
	
	this(string str) {
		this.str = str;
		this.hasBeenRead = false;
	}
	
	@property bool empty() {
		return hasBeenRead;
	}
	
	@property ulong leastSize() {
		return str.length;
	}
	
	@property bool dataAvailableForRead() {
		return !hasBeenRead;
	}
	
	const ubyte[] peek() {
		if (!hasBeenRead)
			return cast(ubyte[]) str;
		else
			return [];
	}
	
	void read(ubyte[] dst) {
		if (!hasBeenRead) {
			dst = cast(ubyte[]) str;
			hasBeenRead = true;
		} else
			dst = null;
	}
}


/**
	Aux class that wraps a std.stream.InputStream into a vibe.core.stream.InputStream.
 */
class StdStreamWrapper : InputStream
{
	private std.stream.InputStream d;
	
	this(std.stream.InputStream stream) {
		this.d = stream;
	}
	
	@property bool empty() {
		return d.eof;
	}
	
	@property ulong leastSize() {
		return d.available;
	}
	
	@property bool dataAvailableForRead() {
		return !d.eof;
	}
	
	const ubyte[] peek() {
		throw new Exception("peek() not implemented in StdStreamWrapper.");
	}
	
	void read(ubyte[] dst) {
		throw new Exception("fucking read() do you wanna read() me?");
		//d.read(dst);
	}
}