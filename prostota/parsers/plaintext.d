module prostota.parsers.plaintext;

public import prostota.parser;
public import prostota.response;

import std.regex;

class PlainTextParser : StringParser
{
	@property string title() const {
		return "Plain text parser";
	}
	
	@property string description() const {
		return q"DESC
This parser either shows text «as is» for PlainTextMode or encodes HTML entities for HtmlMode.

No parsing is done, e.g. the file is considered to contain no links, no images, etc.
DESC";
	}
	
	@property string[] defaultExtensions() const {
		return ["txt"];
	}
	
	Response parseString(string data) {
		// Replace "\n" with "<br/>"
		data = replaceAll(data, regex("\n"), "<br/>\n");
		
		// Wrap into a Response and return
		Runtime.setOpt("mime", "text/plain; charset=utf-8");
		return Response.fromString(data);
	}
}
