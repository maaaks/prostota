module prostota.parsers.raw;

public import prostota.parser;
public import prostota.runtime;

import vibe.core.stream;
import vibe.inet.mimetypes;

class RawParser : StreamParser
{
	@property string title() const {
		return "RAW parser";
	}
	
	@property string description() const {
		return q"DESC
			Pseudoparser which just shows files as is and uses 'mimetype' command to choose MIME type of file.
DESC";
	}
	
	@property string[] defaultExtensions() const {
		return ["", "jpg", "png", "mp3", "css", "png", "gif", "bmp"];
	}
	
	Response parseStream(InputStream data) {
		if (Runtime.sourceFile != "")
			Runtime.setOpt("mime", getMimeTypeForFile(Runtime.sourceFile));
		Runtime.setOpt("decorations", Decorations.None);
		return Response.fromStream(data);
	}
}