module prostota.parsers.native;

public import prostota.exceptions;
public import prostota.parser;

import core.sys.posix.dlfcn;
import std.file;
import std.path;
import std.string;
import vibe.http.server;

class NativeParser : StreamParser
{
	@property string title() const {
		return "Native plugin loader parser";
	}
	
	@property string description() const {
		return "Loads a binary file as a dynamic library and calls its function run(HTTPServerRequest).";
	}
	
	@property string[] defaultExtensions() const {
		return ["so"];
	}
	
	Response parseStream(InputStream) {
		// Load the library
		string libraryPath = Runtime.sourceFile;
		void* library = dlopen(libraryPath.toStringz(), RTLD_LAZY);
		if (!library) throw new Http!500("Can't load "~Runtime.sourceFile);
		
		// Find the function to run
		alias extern(C) Response function(HTTPServerRequest) RunFunction;
		RunFunction run = cast(RunFunction) dlsym(library, "run");
		if (!run) throw new Http!500("Can't find the function.");
		
		// Go to the directory where the library is placed
		string olddir = getcwd();
		chdir(dirName(libraryPath));
		scope(exit) chdir(olddir);
		
		// Run!
		return run(Runtime.request);
	}
}
