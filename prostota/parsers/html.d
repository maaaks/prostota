module prostota.parsers.html;

public import prostota.parser;
public import prostota.runtime;

import std.regex;
import std.string;
import vibe.core.stream;

class HtmlParser : StringParser
{
	@property string title() const {
		return "HTML parser";
	}
	
	@property string description() const {
		return q"DESC
			Parser that shows HTML without any changes, except it recognizes options in the first comment.
DESC";
	}
	
	@property string[] defaultExtensions() const {
		return ["html", "htm"];
	}
	
	Response parseString(string data) {
		Runtime.setOpt("mime", "text/html");
		Runtime.setOpt("decorations", Decorations.None);
		
		// If the file starts with a comment, parse it
		foreach (int l, string line; data.splitLines()) {
			// The opening syntax
			if (l == 0) {
				if (line == "<!--") continue;
				else break;
			}
			
			// The closing syntax
			if (line == "-->") break;
			
			// The key-value pairs
			if (auto m = line.match(regex("^(\\S+):(.*)$"))) {
				Runtime.setOpt(m.captures[1].strip(), m.captures[2].strip());
			}
		}
		
		Runtime.setOpt("mime", "text/html; charset=utf-8");
		return Response.fromString(data);
	}
}
