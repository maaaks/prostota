module prostota.parsers.multimarkdown;

public import prostota.parser;
public import prostota.response;
public import prostota.runtime;

import std.path;
import std.process;
import std.regex;
import std.string;
import std.stream;

class MultiMarkdownParser : PipeBasedParser
{
	@property string title() const {
		return "MultiMarkdown parser";
	}
	
	@property string description() const {
		return q"DESC
Uses MultiMarkdown (from http://fletcherpenney.net/multimarkdown/) to convert text to HTML.
DESC";
	}
	
	@property string[] defaultExtensions() const {
		return ["mmd"];
	}
	
	Response parseString(string data) {
		// Use metadata parsed by Markdown to fill Runtime.opt
		string[] keys = callPipe!(string[])(["multimarkdown", "--metadata-keys"], data);
		foreach (string key; keys) {
			string value = callPipe(["multimarkdown", "--extract", key], data).strip();
			Runtime.setOpt(key, value);
		}
		
		// Now run markdown one more time to get the content
		string directory = Runtime.sourceFile!="" ? dirName(Runtime.sourceFile) : null;
		string output = callPipe([
			"multimarkdown", "--process-html", "--snippet", "--nosmart", "--nolabels", "-a", "-r"],
			data, directory);
		Runtime.setOpt("mime", "text/html; charset=utf-8");
		return Response.fromString(output);
	}
}
