module prostota.parsers.txt2tags;

public import prostota.parser;
public import prostota.response;
public import prostota.runtime;

import std.path;
import std.regex;
import std.string;
import std.stream;

class Txt2TagsParser : PipeBasedParser
{
	@property string title() const {
		return "Parser for txt2tags markup";
	}
	
	@property string description() const {
		return q"DESC
The txt2tags markup (http://txt2tags.org/) is simple and easy. But wait, it's powerful too! You can insert images, make lists, quotes and even tables. All of that using just a few extra characters on your text. No more tag names or funky attributes to remember.

This parser uses /usr/bin/txt2tags binary to parse raw text.
DESC";
	}
	
	@property string[] defaultExtensions() const {
		return ["t2t"];
	}
	
	Response parseString(string data) {
		string raw;
		
		// Use self-made "%opt NAME VALUE" syntax for reading meta information
		foreach (string line; data.splitLines()) {
			if (auto m = match(line, "^%opt\\s+(\\S+)\\s+(.+)\n$"))
				Runtime.setOrAppendOption(m.captures[1], m.captures[2]);
			else
				raw ~= line~"\n";
		}
		
		string directory = Runtime.sourceFile!="" ? dirName(Runtime.sourceFile) : null;
		string output = callPipe(
			["/usr/bin/txt2tags", "--infile=-", "--outfile=-", "--target=html", "--no-headers"],
			raw, directory
		);
		return Response.fromString(output);
	}
}