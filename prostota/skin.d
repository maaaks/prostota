module prostota.skin;

import mustache;
import std.array;
import std.file;
import std.regex;
import std.uri;

public import prostota.config;
public import prostota.filestorage;
public import prostota.response;
public import prostota.runtime;

alias MustacheEngine!(string) Mustache;

Response render(Response input) {
	auto data = new Mustache.Context;
	
	// Main Data
	data["content"] = input.toString();
	
	// Information about current space
	data["space_siteTitle"] = Runtime.space.siteTitle;
	data["space_url"] = Runtime.space.url;
	foreach (string key, string value; Runtime.space.dict) {
		data["dict__"~key] = value;
	}
	
	// Information about other spaces
	// TODO: remove dirty hack «Runtime.storages[0]»
	foreach (Space space; Runtime.space.relatedSpaces) {
		if (getPageInfo(Runtime.request.path, space) !is null) {
			auto translation = data.addSubContext("translations");
			translation["name"] = space.name;
			translation["url"] = space.url ~ Runtime.request.path;
		}
	}
	
	// Current path, encoded for Mustache
	string encodedPath = Runtime.request.path
		.replace("/", "__")
		.replace(".", "_")
		.replace(" ", "_");
	data.useSection("page"~encodedPath);
	
	// All the contents of Runtime.opt as values and as sections
	foreach (string key, string value; Runtime.opts()) {
		data[key] = value;
		data.useSection(key);
	}
	
	// Go!
	Mustache skin;
	string rendered = skin.render(Config.skinsDir~"//m14/m14.html", data);
	return Response.fromString(rendered);
}
