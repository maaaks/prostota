module prostota.config;

import std.path;
import std.regex;
import vibe.d;

public import prostota.space;

class Config
{
	/// All spaces in the system
	static Space[] spaces;
	
	/// The main space (static data will be linked from it)
	static Space favSpace = null;
	
	/// The directory where skins are placed.
	static string skinsDir = null;
	
	shared static initialize(string configFilePath) {
		enum ParseMode { GeneralMode, SpaceMode, DictMode }
		
		// Results of parsing
		Space[string] spacesById;
		string[string][string] dictsById;
		string[][string] relatedSpacesById;
		string favSpaceId = null;
		// Temporary variables for parsing
		ParseMode mode;
		string line = void;
		string id = null;
		
		configFilePath = absolutePath(configFilePath);
		auto ini = new std.stdio.File(configFilePath);
		while ((line = ini.readln()) !is null) {
			line = line.strip();
			
			if (line.empty || line[0] == ';' || line[0] == '#') {
				continue; // it was a comment
			}
			else if (auto m = line.match("\\[\\s*([^\\.]+)\\s*\\]")) {
				// Starting a new Space
				mode = ParseMode.SpaceMode;
				id = m.captures[1];
				spacesById[id] = new Space;
				// If favSpace wasn't defined in general section, the first space becomes it
				if (favSpace is null && favSpaceId is null)
					favSpace = spacesById[id];
			}
			else if (auto m = line.match("\\[\\s*([\\w\\d-_]+)\\.dict\\s*\\]")) {
				// Starting a new dict for a Space (the Space itself may be defined later)
				mode = ParseMode.DictMode;
				id = m.captures[1];
				dictsById[id] = string[string].init;
			}
			else if (mode == ParseMode.GeneralMode) {
				// Readiing syystem-wide options
				
				if (auto m = line.match("favSpace\\s*=\\s*(.*)")) {
					favSpaceId = m.captures[1];
				}
				else if (auto m = line.match("skinsDir\\s*=\\s*(.*)")) {
					skinsDir = buildNormalizedPath(absolutePath(m.captures[1], dirName(configFilePath)));
				}
			}
			else if (mode == ParseMode.SpaceMode) {
				// Reading something for current space
				
				if (auto m = line.match("url\\s*=\\s*(.*)")) {
					spacesById[id].url = m.captures[1];
				}
				else if (auto m = line.match("urlPattern\\s*=\\s*(.*)")) {
					spacesById[id].urlPattern = m.captures[1];
				}
				else if (auto m = line.match("siteTitle\\s*=\\s*(.*)")) {
					spacesById[id].siteTitle = m.captures[1];
				}
				else if (auto m = line.match("name\\s*=\\s*(.*)")) {
					spacesById[id].name = m.captures[1];
				}
				else if (auto m = line.match("root\\s*=\\s*(.*)")) {
					spacesById[id].root = buildNormalizedPath(absolutePath(m.captures[1], dirName(configFilePath)));
				}
				else if (auto m = line.match("relatedSpaces\\s*=\\s*(.*)")) {
					relatedSpacesById[id] = m.captures[1].split(",");
					foreach (ref relId; relatedSpacesById[id])
						relId = relId.strip();
				} else
					throw new ConfigParseError(line);
			}
			else if (mode == ParseMode.DictMode) {
				// Reading an item for current dict
				
				if (auto m = line.match("(\\S+)\\s*=\\s*(.*)")) {
					dictsById[id][m.captures[1]] = m.captures[2];
				} else
					throw new ConfigParseError(line);
			} else
				throw new ConfigParseError(line);
		}
		
		// Copying spaces and their dictionaries into static array;
		// cross-linking related spaces
		foreach (string id, Space space; spacesById) {
			spaces ~= space;
			if (id in dictsById)
				space.dict = dictsById[id];
			foreach (string relId; relatedSpacesById[id])
				space.relatedSpaces ~= spacesById[relId];
		}
		if (favSpace is null && favSpaceId !is null)
			favSpace = spacesById[favSpaceId];
	}
}

class ConfigParseError : Exception
{
	this(string line) {
		super("Incorrect config line: "~line);
	}
}
