module prostota.directory;

import std.array;
import std.file;
import std.path;
import std.regex;
import std.string;


/**
	Represents a content directory and its properties.
	
	In content file structure, a directory with its name and optional hidden configuration files
	influences on all the files inside it and its subdirectories.
	Moreover, rendering a file sometimes depends on directory-wide data,
	e.g. «prev—next»-navigation depends on sibling files of current file.
	That's why it's important to have a convenient way for accessing
	a directory's properties, and Directory class is this way.
	
	When initializing, Directory analyzes all 
 */
class Directory
{
	enum Mode {
		Normal,
		Final,
		Forbidden,
	}
	
	immutable string path;
	immutable Mode mode;
	
	this(in string path) {
		// Temporary value will be used to initialize immutable fields
		Mode tmpMode;
		scope(exit) mode = tmpMode;
		
		// Set the path with trailing slash
		if (path[$-1] == '/')
			this.path = path;
		else
			this.path = path[0..$-1];
		
		// Set mode to Forbidden when name starts with a dot
		string base = baseName(path);
		if (base[0] == '.')
			tmpMode = Mode.Forbidden;
		
		// Read params from .params file
		string paramsFileName = path~".params";
		if (exists(paramsFileName)) {
			auto ini = new std.stdio.File(paramsFileName);
			
			// Read by line
			string line = void;
			while ((line = ini.readln()) !is null) {
				line = line.strip();
				
				if (line.empty || line[0] == ';' || line[0] == '#') {
					continue; // it was a comment
				}
				else if (auto m = line.match("mode\\s*=\\s*Normal")) {
					tmpMode = Mode.Normal;
				}
				else if (auto m = line.match("mode\\s*=\\s*Final")) {
					tmpMode = Mode.Final;
				}
				else if (auto m = line.match("mode\\s*=\\s*Forbidden")) {
					tmpMode = Mode.Forbidden;
				}
			}
		}
	}
}