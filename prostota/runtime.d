module prostota.runtime;

public import prostota.space;

public import prostota.parsers.html;
public import prostota.parsers.multimarkdown;
public import prostota.parsers.native;
public import prostota.parsers.plaintext;
public import prostota.parsers.raw;
public import prostota.parsers.txt2tags;

import std.path;
import vibe.http.server;


enum Decorations : string {
	Full = "full",
	Basic = "basic",
	None = "none",
}


class Runtime
{
	// Information about loaded modules
	static Parser[] parsers;             /// Available parser plugins.
	
	// The request information
	static HTTPServerRequest request;    /// The request being processed.
	static string pseudoPath;        /// The pseudopath part of requests (if any).
	
	// Information about what is done
	static string sourceFile;            /// Path to file that was used to load content.
	
	// Options for rendering
	static Space space = null;           /// Current space.
	static string[string] headers;       /// Headers to be sent to client.
	
	// Internal fields
	static private Parser[string] _parsersByExt;  /// Parsers by file extension.
	static private string[string] _opts;          /// Custom runtime options, mainly for metadata set by parser.
	
	
	static this() {
		// Load parsers
		parsers ~= new HtmlParser;
		parsers ~= new MultiMarkdownParser;
		parsers ~= new NativeParser;
		parsers ~= new PlainTextParser;
		parsers ~= new RawParser;
		parsers ~= new Txt2TagsParser;
		foreach (Parser parser; parsers)
			foreach (string ext; parser.defaultExtensions)
				_parsersByExt[ext] = parser;
		
		// Runtime options
		_opts["decorations"] = Decorations.Full;
		sourceFile = "";
	}
	
	
	static void reset() {
		space = null;
		_opts = _opts.init;
		headers = headers.init;
	}
	
	
	static Parser getParserForExt(string ext) {
		auto parser = _parsersByExt.get(ext, null);
		if (parser !is null)
			return parser;
		else
			return null;
	}
	
	static Parser getParserForCurrentExt() {
		if (sourceFile != "") {
			string ext = extension(sourceFile);
			if (ext.length > 0 && ext[0] == '.')
				ext = ext[1..$];
			return getParserForExt(ext);
		} else
			return null;
	}
	
	static void applyHeaders(HTTPServerResponse httpRes) {
		foreach (string key, string value; headers)
			httpRes.headers[key] = value;
	}
	
	static string[string] opts() {
		return _opts;
	}
	
	static string opt(in string key, in string defaultValue="") {
		return _opts.get(key, defaultValue);
	}
	
	static bool hasOpt(in string key) {
		return (key in _opts) !is null;
	}
	
	static void setOpt(in string key, in string value) {
		_opts[key] = value;
	}
	
	static void setOrAppendOption(in string key, in string value) {
		if (key in _opts)
			_opts[key] ~= "\n"~value;
		else
			_opts[key] = value;
	}
}
