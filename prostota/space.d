module prostota.space;

class Space
{
	string id;            /// Short identitfier («en», «ru», etc.) for dirty hacks.
	string url;           /// Base URL of this version of the site, without trailing slash.
	string urlPattern;    /// Pattern describing urls for which this scope apply.
	string siteTitle;     /// Site title as shown on the main page and in links to it.
	string name;          /// Space title as shown in language switcher, usually just the name of language.
	string root;          /// Path to directory where content of this scope is stored.
	string[string] dict;  /// Custom data for using in templates.
	
	/**
	 * Spaces that should be shown in language switcher.
	 * 
	 * For instance, I have three domains: Maaaks.ru, Maaaks.me, Ololo.im.
	 * Maaaks.ru and Maaaks.me are very similar and should be cross-linked,
	 * so Maaaks.ru has Maaaks.me in relatedScopes, and vice versa.
	 * Ololo.im is an independent project, so it doesn't have these in relatedScopes.
	 * (In fact, it has Ololo.im/ru/ in relatedScopes.)
	 */
	Space[] relatedSpaces;
}