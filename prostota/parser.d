module prostota.parser;

public import prostota.exceptions;
public import prostota.response;

import std.conv;
import std.file;
import std.process;
import std.stdio;
import vibe.core.stream;

interface Parser
{
	/// Short title of the parser.
	@property string title() const;
	
	/// Long description or howto about the parser.
	@property string description() const;
	
	/**
		Default set of file extensions to be handled by this parser.
		
		Non-empty extensions must be stored without dot,
		empty expression must be stored as an empty string.
		
		User can redefine this in Space configuration.
		
		Examples:
			---
			["html", "htm"]
			[""]
			---
	 */
	@property string[] defaultExtensions() const;
	
	/**
		Checks whether given extension exists in defaultExtensions() list.
	 */
	final bool supportsExtension(string ext) {
		foreach (string supportedExt; defaultExtensions()) {
			if (supportedExt == ext)
				return true;
		}
		return false;
	}
	
	final Response parse(Response response) {
		// Call parseString() or parseStream() based on Response type
		if (typeid(response) == typeid(StringResponse)) {
			auto r = cast(StringResponse) response;
			return parseString(r.toString());
		}
		else if (typeid(response) == typeid(StreamResponse)) {
			auto r = cast(StreamResponse) response;
			return parseStream(r.toStream());
		}
		else
			throw new Exception("Unknown Response type: "~typeid(response).toString());
	}
	
	Response parseString(string data);
	Response parseStream(InputStream data);
}


abstract class StringParser : Parser
{
	final Response parseStream(InputStream data) {
		// Read the whole stream to a single string
		string result;
		while (data.leastSize > 0) {
			auto sizeToRead = std.math.fmin(data.leastSize, 4096);
			ubyte[] buffer = new ubyte[ to!ulong(sizeToRead) ];
			data.read(buffer);
			result ~= cast(string) buffer;
		}
		
		// Call the parse() method for the string
		return parseString(result);
	}
}


abstract class PipeBasedParser : StringParser
{
	/**
		Execute command in given directoory, pipe given input to it and return whole output.
		Call it with T="string" or T="array" based on which type you prefer to be returned.
	 */
	final protected T callPipe(T=string)(in string[] commandAndArgs, in string input, in string directory=null)
	if (is(T == string) || is(T == string[])) {
		// Temporarily chdir to a directory if given
		immutable string olddir = getcwd();
		scope(exit) chdir(olddir);
		if (directory !is null)
			chdir(directory);
		
		// Create pipes object
		auto pipes = pipeProcess(commandAndArgs, Redirect.stdin|Redirect.stdout);
		
		// Write the whole input
		pipes.stdin.write(input);
		pipes.stdin.close();
		
		// Read the whole output to string or array
		static if (is(T == string)) {
			string output;
			foreach (char[] line; pipes.stdout.byLine(KeepTerminator.yes))
				output ~= line;
			return output;
		} else {
			string[] output;
			foreach (char[] line; pipes.stdout.byLine(KeepTerminator.no))
				output ~= to!string(line);
			return output;
		}
	}
}


abstract class StreamParser : Parser
{
	final Response parseString(string data) {
		auto wrapper = new StringStreamingWrapper(data);
		return parseStream(wrapper);
	}
}