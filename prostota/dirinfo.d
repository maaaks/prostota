module prostota.dirinfo;

import std.array;
import std.bitmanip;
import std.conv;
import std.file;
import std.path;
import std.regex;
import std.string;

/**
	Represents a content directory and its properties.
	
	In content file structure, a directory with its name and optional hidden
	configuration files influences on all the files inside it and its subdirectories.
	Moreover, rendering a file sometimes depends on directory-wide data,
	e.g. «prev—next»-navigation depends on sibling files of current file.
	That's why it's important to have a convenient way for accessing
	a directory's properties, and DirInfo class is this way.
	
	DirInfo is usually initialized with static method DirInfo::load() which accepts
	a relative path to a directory, e.g.
 */
//class DirInfo
//{
	
	
//	string path;
//	string fallbackPage;
//	string[string] customData;
	
//	/**
//		Creates DirInfo object for given path.
		
//		As a microoptimization, additional baseName parameter can be passed
//		if it is already known when calling the constructor.
//		Otherwise, it will be retrieved as baseName(path).
//	 */
//	this(in string path, string base=null) {
//		if (base is null) base = baseName(path);
//		this.path = path;
		
//		// Try to detect directory's mode based on its basename and files such as _magic.*
//		if (base[0] == '.')
//			mode = PageInfo.Mode.Hidden;
//		else if (base[0] == '_')
//			mode = Mode.Forbidden;
//		else if (exists(path~"~/_magic."))
		
		
//		// Is there a configuration file? Read it.
//		immutable string configFilePath = path~"/.dirinfo";
//		if (exists(configFilePath) && isFile(configFilePath))
//			readProperties(readText(configFilePath));
		
//		// 
		
//		// Temporary value will be used to initialize immutable fields
//		Mode tmpMode;
//		scope(exit) mode = tmpMode;
		
//		// Set the path with trailing slash
//		if (path[$-1] == '/')
//			this.path = path;
//		else
//			this.path = path[0..$-1];
		
//		// Set mode to Forbidden when name starts with a dot
//		if (base[0] == '.')
//			tmpMode = Mode.Forbidden;
//	}
	
	
//	void readProperties(in string rawData) {
//		foreach (line; rawData.splitLines()) {
//			if (auto m = line.match(regex("^\\s* ([^=]+) \\s* = \\s* (.*) \\s*$", "x"))) {
//				immutable string key = m.captures[1];
//				immutable string value = m.captures[2];
				
//				switch (key) {
//				case "Mode":
//					mode = to!Mode(value);
//					break;
//				case "FallbackPage":
//					fallbackPage = buildNormalizedPath(path, value);
//					break;
//				default:
//					customData[key] = value;
//				}
//			}
//		}
//	}
//}