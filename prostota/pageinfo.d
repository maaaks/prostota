module prostota.pageinfo;

import std.path;

public import prostota.runtime;

enum PageMode {
	Normal,     /// File that will be read directly
	Hidden,     /// File or directory that should throw Http!404 for any requests as it would not exist
	Forbidden,  /// File or directory that should throw Http!403 for any requests
	Fallback,   /// File was not requested directly but used as a fallback
}

/**
	Represents information about a single page.
 */
class PageInfo {
	immutable string path;     /// URL part as given.
	string   file;             /// Path to file where the content is placed
	string   ext;              /// File extension
	bool     unslashed;        /// When true, page's URL must be shown without trailing slash
	PageMode mode;             /// How to process requests to these file/folder
	
	string[string] customData; /// Additional meta information for template engines, etc.
	
	/**
		The constructor is used only for setting request.
	 */
	this(in string path) {
		this.path = path;
	}
}