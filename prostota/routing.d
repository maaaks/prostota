module prostota.routing;

import std.conv;
import std.file;
static import std.regex;
import std.stdio;
import std.string;
import vibe.http.server;

public import prostota.config;
public import prostota.exceptions;
public import prostota.filestorage;
public import prostota.parser;
public import prostota.response;
public import prostota.runtime;
public import prostota.skin;

void index(HTTPServerRequest request, HTTPServerResponse httpRes) {
	try {
		Runtime.request = request;
		
		// Reset runtime data from previous query
		Runtime.reset();
		
		// Choose space
		string urlString = request.fullURL.toString();
		foreach (Space space; Config.spaces) {
			auto r = std.regex.regex(space.urlPattern);
			if (std.regex.match(urlString, r))
				Runtime.space = space;
		}
		if (Runtime.space is null)
			Runtime.space = Config.favSpace;
		
		// Get page info
		PageInfo info = getPageInfo(request.path);
		// If no page found, maybe we should redirect user to a Space where such page exists
		if (info is null) {
			foreach (Space relSpace; Runtime.space.relatedSpaces) {
				info = getPageInfo(request.path, relSpace);
				if (info !is null)
					throw new Http!302(getFullCanonicalUrl(info, relSpace));
			}
		}
		
		// Load page and its options
		Response response = getResponse(request.path);
		foreach (key, value; info.customData)
			Runtime.setOpt(key, value);
		
		// Maybe we should make redirect to canonical URL of the page
		immutable string canonical = info.getCanonicalPath();
		if (info.mode != PageMode.Fallback && request.path != canonical)
			throw new Http!302(info.getFullCanonicalUrl());
		
		// Choose parser for this page
		Parser parser = Runtime.getParserForCurrentExt();
		response = parser.parse(response);
		
		// Use skin
		string decorations = Runtime.opt("decorations");
		writeln(Decorations.Full);
		if (decorations == "" || decorations.icmp(Decorations.Full))
			response = render(response);
		else if (decorations.icmp(Decorations.Basic))
			response = Response.fromString(
				"<!DOCTYPE html>\n<html>\n"
				"<head>\n"
				"<meta charset=\"utf-8\"/>"
				"<title>"~Runtime.opt("title")~"</title>\n"
				"</head>\n"
				"<body>\n"~response.toString()~"</body>\n"
				"</html>");
		
		// Send data to browser
		httpRes.statusCode = (info.mode==PageMode.Fallback) ? 404 : 200;
		httpRes.contentType = Runtime.opt("mime");
		response.applyToHttp(httpRes);
	}
	catch (HttpException e) {
		httpRes.statusCode = e.code;
		httpRes.contentType = "text/html; charset=utf-8";
		httpRes.bodyWriter.write("<h1>"~to!string(e.code)~" "~e.msg~"</h1>");
	}
	catch(HttpRedirect r) {
		httpRes.redirect(r.msg, r.code);
	}
	catch(Exception e) {
		httpRes.statusCode = 500;
		httpRes.contentType = "text/html; charset=utf-8";
		httpRes.bodyWriter.write("<h1>500 Internal Server Error</h1>\n");
		httpRes.bodyWriter.write(e.msg~"<br/>\n<br/>\n");
		
		for (Throwable t=e; t !is null; t=t.next)
			httpRes.bodyWriter.write("<code style='color:darkgreen;'>&nbsp; "~t.file~":"~to!string(t.line)~"</code><br/>\n");
		
		foreach (string infoLine; e.info.toString().splitLines())
			if (std.regex.match(infoLine, "prostota\\."))
				httpRes.bodyWriter.write("<code style='color:brown;'>&nbsp; "~infoLine~"</code><br/>\n");
			else
				httpRes.bodyWriter.write("<code style='color:gray;'>&nbsp; "~infoLine~"</code><br/>\n");
	}
}
