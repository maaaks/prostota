module prostota.filestorage;

import std.array;
import std.conv;
import std.file;
import std.path;
import std.regex;
import std.string;

public import prostota.dirinfo;
public import prostota.pageinfo;
public import prostota.response;


////////
// Important convention: never use trailing slash in paths in this file.
// E.g., dir~"/file.txt" must always produce a valid path without double slashes.
////////


/**
	Return content of the page by its path or throw an HttpException.
 */
Response getResponse(in string path) {
	PageInfo info = getPageInfo(path);
	
	if (info is null || info.mode == PageMode.Hidden)
		throw new Http!404;
	
	if (info.mode == PageMode.Forbidden)
		throw new Http!403;
	
	Runtime.sourceFile = info.file;
	return Response.fromFile(info.file);
}


/**
	Searches files according to rules described in getResponse()'s comment.
	Notice that a hidden file can be returned, too. A forbidden file can't.
	If there is really no file for given request, null is returned.
	
	This method never causes HTTP errors or other exceptions but just returns PageInfo,
	so it can be called from any places where you just need to get page information.
	(For example, it is called to check for available translations.)
	
	The implementation uses PageInfo to collect information but returns a PageInfo.
 */
PageInfo getPageInfo(in string path, in Space space=Runtime.space) {
	immutable string base = baseName(path);
	
	// Prepare objects for temporary information
	PageInfo info = new PageInfo(path);
	PageInfo fallback = new PageInfo(path);
	fallback.mode = PageMode.Fallback;
	
	// Prepare path parts, remove starting and trailing empty parts
	string[] parts = path.buildNormalizedPath().split(dirSeparator);
	if (parts[0] == "")
		parts = parts[1 .. $];
	if (parts[$-1] == "")
		parts = parts[0 .. $-1];
	if (parts.length == 0)
		parts ~= "";
	
	// We start at the Space's root and will walk deeper
	string currentPath = space.root;
	
	/*
	Go down from the root directory of the Space, updating the PageInfo object on each level.
	Note that at any moment the real file structure may end with a file (not directory),
	and remaining parts of the request may be just given to the file as a request.
	*/
	foreach (partIndex, part; parts) {
		/*
		Find the real name of current path (maybe with a prefix and/or extension).
		For example, for «.../css/style.css» it may find «.../css.files/style.css»
		(that may be later processed as a special static-files-like folder or so).
		If something will be found, the real file/dir name will be in info.file.
		*/
		if (!tryAllPrefixesAndExtensions(info, currentPath, part)) {
			// Try to use _fallback* instead of requested file.
			return fallback.file ? fallback : null;
		}
		
		/*
		Further logic depends on is current place a file or a directory
		*/
		currentPath ~= dirSeparator ~ part;
		if (exists(currentPath) && isDir(currentPath))
		{
			// Directory can contain file _options.ini overriding some parameters
			parseOptionsIni(info, fallback, currentPath);
			
			// Directory can contain file _magic* which will process any request.
			// Its extension may be any of known extensions (including empty extension),
			// so we use usual file choosing function.
			// If magic is found, no further file search is needed.
			if (tryAllExtensions(fallback, currentPath ~ dirSeparator ~ "_magic"))
				break;
			
			// Directory may also provide _fallback* file to use when nothing found.
			// If such file is provided, remember it as «fallback» for possible use in future.
			tryAllExtensions(fallback, currentPath ~ dirSeparator ~ "_fallback");
			
			// If directory's mode is Forbidden, don't walk deeper anymore
			if (info.mode == PageMode.Forbidden)
				return null;
			
			// If the directory is last part of path, we need an index file (we can't return a directory).
			// It may be named as the directory itself prefixed with an underscore (e.g., "/foo/_foo.mmd")
			// or just «_index*» (e.g. «/foo/_index.mmd»). Former is more preferrable.
			if (partIndex == parts.length-1) {
				if (tryAllExtensions(info, currentPath ~ dirSeparator ~ "_" ~ part))
					return info;
				else if (tryAllExtensions(info, currentPath ~ dirSeparator ~ "_index"))
					return info;
				else
					return fallback.file ? fallback : null;
			}
		}
		else
		{
			// So, current part is a file.
			// If it's the las part of path, we just use it.
			if (partIndex == parts.length-1)
				return info;
			
			// Else, we need to pass all the remaining part of path to the file.
			// For example, to process requests like «/forum/topic12345», user can have just a «/forum».
			// The  «topic12345» part (without starting slash) will be assigned to Runtime.pseudoPath.
			Runtime.pseudoPath = parts[partIndex+1 .. $-1].join(dirSeparator);
		}
	}
	
	// If nothing was found, return null
	return null;
}



/**
	Tries to read directory-level config from _options.ini file.
	
	Requires both "info" and "fallback" since each of them may be modified by config.
	Path to the directory is passed as "path".
 */
private void parseOptionsIni(PageInfo info, PageInfo fallback, in string path) {
	// Check if the config exists
	immutable string configFilePath = path ~ dirSeparator ~ "_options.ini";
	if (!exists(configFilePath) || !isFile(configFilePath)) return;
	
	// Read text
	immutable string configRawText = readText(configFilePath);
	
	// Directory's configuration file can override these settings
	foreach (line; configRawText.split("\n")) {
		if (auto m = line.match(regex("^\\s* ([^=]+) \\s* = \\s* (.*) \\s*$", "x"))) {
			immutable string key = m.captures[1].strip();
			immutable string value = m.captures[2].strip();
			
			switch (key) {
			case "Mode":
				info.mode = to!PageMode(value);
				break;
			case "FallbackPage":
				fallback.file = buildNormalizedPath(path, value);
				break;
			default:
				info.customData[key] = value;
			}
		}
	}
}


/**
	Checks existence of given file/dir with all possible prefixes («_», «.», «._») or without prefix.
	If any of the files exists, the function returns true, and the PageInfo contains info about file.
	Otherwise, the function returns false.
 */
private static bool tryAllPrefixesAndExtensions(PageInfo info, in string dir, in string base) {
	if (tryAllExtensions(info, dir~dirSeparator~base))      { info.mode = PageMode.Normal; return true; }
	if (tryAllExtensions(info, dir~dirSeparator~"_"~base))  { info.mode = PageMode.Forbidden; return true; }
	if (tryAllExtensions(info, dir~dirSeparator~"."~base))  { info.mode = PageMode.Hidden; return true; }
	if (tryAllExtensions(info, dir~dirSeparator~"._"~base)) { info.mode = PageMode.Hidden; return true; }
	return false;
}


/**
	Checks whether given file can be found with any known extensions.
	If yes, constructs PageInfo and returns true.
	If no, returns false.
 */
private static bool tryAllExtensions(PageInfo info, in string pathWithoutExt) {
	// Collect all available extensions into array
	static string[] extensions = null;
	if (extensions is null) {
		// Empty extension
		extensions = [""];
		// All extensions defined by all parsers
		foreach (parser; Runtime.parsers)
			foreach (ext; parser.defaultExtensions)
				extensions ~= ext;
	}
	
	// Iterate through all extensions
	foreach (ext; extensions) {
		// There are two versions of extension: with one dot and with two dots.
		// Latter is for «unslashed» pages (their path must be shown without trailing slash).
		// For empty extension, the two versions are just «» and «.», respectively.
		immutable string normalPath    = pathWithoutExt ~ (ext!="" ? "."~ext  : "");
		immutable string unslashedPath = pathWithoutExt ~ (ext!="" ? ".."~ext : ".");
		
		// Try find normal file path
		if (exists(normalPath)) {
			if (info !is null) {
				info.file = normalPath;
				info.ext = ext;
			}
			return true;
		}
		
		// Try find unslashed file path
		if (exists(unslashedPath)) {
			if (info !is null) {
				info.file = unslashedPath;
				info.ext = ext;
				info.unslashed = true;
			}
			return true;
		}
	}
	
	// Nothing found
	return false;
}


/**
	Return canonical path part for URL of given PageInfo.
	Examples that can be done by this function depending on configuration:
	- /foo/bar/   => /foo/bar
	- /foo/bar    => /foo/bar/
	- /foo/alias  => /foo/bar/
	
	Currently doesn't allow to define custom logic for magic files.
 */
static string getCanonicalPath(PageInfo info) {
	string path = info.path;
	
	// Add or remove slash
	if (info.unslashed && path[$-1] == '/')
		path = path[0 .. $-1];
	else if (!info.unslashed && path[$-1] != '/')
		path ~= '/';
	
	// Ready
	return path;
}


/** Constructs the full canonical URL for the page in given Space.
	Includes protocol and host.
 */
string getFullCanonicalUrl(PageInfo info, Space space=Runtime.space) {
	return space.url ~ getCanonicalPath(info);
}